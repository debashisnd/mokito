package com.cloudleaf.cloudos;


import com.cloudleaf.cloudos.model.Customer;
import com.cloudleaf.cloudos.repository.CustomerRepository;
import com.cloudleaf.cloudos.service.CustomerServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerService = new CustomerServiceImpl();

    Optional<Customer> customer;
    List<Customer> list_customer;

    @BeforeEach
    void setMockOutput() {
        Customer c1 = new Customer("1", "debashis", "dnandi@cloudleaf.io",
                "88603825421333", "77766554" );
        this.customer = Optional.of(c1);
        List<Customer>  lstCustomer = new ArrayList<>( );
        lstCustomer.add(c1);
        this.list_customer = lstCustomer;
        
    }

    @DisplayName("test_getCustomerById")
    @Test
    void test_getCustomerById() {
        when(customerRepository.getCustomerById(any())).thenReturn(this.customer);
        Assertions.assertEquals(this.customer, customerService.getCustomerById(any()));
    }

    @DisplayName("test_getAllCustomer")
    @Test
    void test_getAllCustomer() {
        when(customerRepository.getAllCustomer()).thenReturn(this.list_customer);
        Assertions.assertEquals(this.list_customer, customerService.getAllCustomer());
    }
}
