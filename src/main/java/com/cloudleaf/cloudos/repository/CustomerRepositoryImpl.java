package com.cloudleaf.cloudos.repository;

import com.cloudleaf.cloudos.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

     List<Customer> lstCusromer = Arrays.asList(
            new Customer("1", "Debashis", "ddnn81@gmail.com", "8860382542", "88809"),
            new Customer("2", "Devdeep", "devdeep@gmail.com", "8860777777", "77666"));

    @Override
    public List<Customer> getAllCustomer() {
        return lstCusromer;
    }

    @Override
    public Optional<Customer> getCustomerById(String id) {
        Optional<Customer> customer = lstCusromer.stream().filter(i -> i.getId().equals(id)).findFirst();
        return customer;
    }
}
