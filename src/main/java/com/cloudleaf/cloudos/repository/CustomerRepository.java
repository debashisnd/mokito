package com.cloudleaf.cloudos.repository;

import com.cloudleaf.cloudos.model.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository {
    List<Customer> getAllCustomer();
   Optional<Customer> getCustomerById(String idxy);
}
