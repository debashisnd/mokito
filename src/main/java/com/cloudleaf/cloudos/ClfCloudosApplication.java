package com.cloudleaf.cloudos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClfCloudosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClfCloudosApplication.class, args);
	}

}
