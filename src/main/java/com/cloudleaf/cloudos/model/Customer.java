package com.cloudleaf.cloudos.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder

public class Customer {
    private String id;
    private String name;
    private String email;

    private String phone;
    private String fax;
}
