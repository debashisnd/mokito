package com.cloudleaf.cloudos.service;

import com.cloudleaf.cloudos.model.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> getAllCustomer();
    Optional<Customer> getCustomerById(String id);
}
