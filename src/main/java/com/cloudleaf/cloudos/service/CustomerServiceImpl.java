package com.cloudleaf.cloudos.service;

import com.cloudleaf.cloudos.model.Customer;
import com.cloudleaf.cloudos.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements  CustomerService{

    @Autowired
    CustomerRepository customerRepository;

    public CustomerServiceImpl() {}

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Optional<Customer> getCustomerById(String id) {
        return customerRepository.getCustomerById(id);
    }
}
