package com.cloudleaf.cloudos;

import com.cloudleaf.cloudos.model.Customer;
import com.cloudleaf.cloudos.service.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/v1/api/")
public class CustomerController {

    @Autowired
    CustomerServiceImpl customerService;

    @GetMapping("/customer")
    public ResponseEntity<?> getAllCustomer() {
        return ResponseEntity.ok(customerService.getAllCustomer());
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<?> getAllCustomer(@PathVariable("id") String id) {
        Optional<Customer> customer = customerService.getCustomerById(id);
        if (customer != null)
            return ResponseEntity.ok(customer);
        else
            return (ResponseEntity<?>) ResponseEntity.notFound();

    }


    @GetMapping("/test")
    public String test() {
        return "ok";
    }
}
